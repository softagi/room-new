package softagi.mansour.roomwithbottom.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import softagi.mansour.roomwithbottom.models.userModel;


@Database(entities = {userModel.class}, version = 1)
public abstract class userDatabase extends RoomDatabase
{
    public abstract userDao userDao();
}