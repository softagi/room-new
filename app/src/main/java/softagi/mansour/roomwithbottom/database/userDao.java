package softagi.mansour.roomwithbottom.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import softagi.mansour.roomwithbottom.models.userModel;

@Dao
public interface userDao
{
    @Insert
    void insertUser(userModel userModel);

    @Query("SELECT * FROM usersTable")
    List<userModel> getUsers();
}