package softagi.mansour.roomwithbottom.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import java.util.List;

import softagi.mansour.roomwithbottom.R;
import softagi.mansour.roomwithbottom.database.userDatabase;
import softagi.mansour.roomwithbottom.models.userModel;
import softagi.mansour.roomwithbottom.onNavigate;

public class usersFragment extends Fragment
{
    private View mainView;
    private RecyclerView recyclerView;
    private userDatabase userDatabase;

    private onNavigate onNavigate;

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        onNavigate = (onNavigate) context;
        onNavigate.onChange("users");
    }

    @Override
    public void onResume()
    {
        super.onResume();
        onNavigate.onChange("users");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_users, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initRecycler();
        initDB();
        new getUsers().execute();
    }

    private void initDB()
    {
        userDatabase = Room.databaseBuilder(getContext(), userDatabase.class, "userDatabase").build();
    }

    private void initRecycler()
    {
        recyclerView = mainView.findViewById(R.id.users_recycler);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    class usersAdapter extends RecyclerView.Adapter<usersAdapter.VH>
    {
        List<userModel> models;

        public usersAdapter(List<userModel> models)
        {
            this.models = models;
        }

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.user_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position)
        {
            userModel userModel = models.get(position);

            String name = userModel.getName();
            String address = userModel.getAddress();
            String mobile = userModel.getMobile();

            holder.nameText.setText(name);
            holder.addressText.setText(address);
            holder.mobileText.setText(mobile);
        }

        @Override
        public int getItemCount()
        {
            return models.size();
        }

        class VH extends RecyclerView.ViewHolder
        {
            TextView nameText,addressText,mobileText;

            public VH(@NonNull View itemView)
            {
                super(itemView);

                nameText = itemView.findViewById(R.id.user_name_text);
                addressText = itemView.findViewById(R.id.user_address_text);
                mobileText = itemView.findViewById(R.id.user_mobile_text);
            }
        }
    }

    class getUsers extends AsyncTask<Void, Void, List<userModel>>
    {
        @Override
        protected List<userModel> doInBackground(Void... voids)
        {
            return userDatabase.userDao().getUsers();
        }

        @Override
        protected void onPostExecute(List<userModel> userModels)
        {
            recyclerView.setAdapter(new usersAdapter(userModels));
        }
    }
}