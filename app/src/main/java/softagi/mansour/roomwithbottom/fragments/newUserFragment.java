package softagi.mansour.roomwithbottom.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import softagi.mansour.roomwithbottom.R;
import softagi.mansour.roomwithbottom.database.userDatabase;
import softagi.mansour.roomwithbottom.models.userModel;
import softagi.mansour.roomwithbottom.onNavigate;

public class newUserFragment extends Fragment
{
    private View mainView;
    private EditText userNameField;
    private EditText userAddressField;
    private EditText userMobileField;
    private Button addBtn;

    private userDatabase userDatabase;

    private onNavigate onNavigate;

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        onNavigate = (onNavigate) context;
        onNavigate.onChange("newUser");
    }

    @Override
    public void onResume()
    {
        super.onResume();
        onNavigate.onChange("newUser");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_new_user, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initViews();
        initDB();
        addUser();
    }

    private void initDB()
    {
        userDatabase = Room.databaseBuilder(getContext(), userDatabase.class, "userDatabase").build();
    }

    private void addUser()
    {
        addBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String name = userNameField.getText().toString();
                String address = userAddressField.getText().toString();
                String mobile = userMobileField.getText().toString();

                if (name.isEmpty() || address.isEmpty() || mobile.isEmpty())
                {
                    Toast.makeText(getContext(), "invalid data", Toast.LENGTH_SHORT).show();
                    return;
                }

                userModel userModel = new userModel(name,address,mobile);
                insertUser(userModel);
            }
        });
    }

    private void insertUser(userModel userModel)
    {
        new insertUserToDB().execute(userModel);
    }

    private void initViews()
    {
        userNameField = mainView.findViewById(R.id.user_name_field);
        userAddressField = mainView.findViewById(R.id.user_address_field);
        userMobileField = mainView.findViewById(R.id.user_mobile_field);
        addBtn = mainView.findViewById(R.id.add_btn);
    }

    class insertUserToDB extends AsyncTask<userModel, Void, Void>
    {
        @Override
        protected Void doInBackground(userModel... userModels)
        {
            userDatabase.userDao().insertUser(userModels[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            Log.d("user", "is added successfully");
        }
    }
}