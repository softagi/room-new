package softagi.mansour.roomwithbottom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import softagi.mansour.roomwithbottom.fragments.newUserFragment;
import softagi.mansour.roomwithbottom.fragments.usersFragment;

public class MainActivity extends AppCompatActivity implements onNavigate
{
    private TextView textView;

    private Boolean isUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.fragment_text);
        loadFragment();
    }

    void loadFragment()
    {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container,new usersFragment())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        if (isUser)
        {
            menuInflater.inflate(R.menu.main_menu, menu);
            Log.d("menu is user", isUser.toString());
        } else
            {
                menuInflater.inflate(R.menu.new_user_menu, menu);
                Log.d("menu is user", isUser.toString());
            }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.add_new_user:
                Log.d("menu item", "clicked");

//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.fragment_container, new newUserFragment())
//                        .addToBackStack(null)
//                        .commit();

                userDialog();

                break;
            case R.id.done_user:

                Log.d("done menu item", "clicked");

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onChange(String fragment)
    {
        switch (fragment)
        {
            case "users":
                textView.setText("users");
                isUser = true;
                Log.d("on change is user", isUser.toString());
                break;
            case "newUser":
                textView.setText("new user");
                isUser = false;
                Log.d("on change is user", isUser.toString());
                break;
        }
    }

    public void userDialog()
    {
        Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.user_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.show();
    }
}